<div class="row">
    <div class="col s12">
        <div class="col s4">
            <a href="/home"><img src="{{url('/images/logo.png')}}" class="img-responsive" height="100" width="200"></a>
        </div>
        <div class="col s3 m3 offset-m5 offset-s7">
            <div class="input-field" style="margin-top: 2rem;">
                <input style="border: 1px solid black; background-color: #024b58; color: #fff;"
                       id="search2" type="search" required
                       placeholder="Enter your Mail">
                <i class="material-icons" style="left: 0.5rem!important;color: white;top: 7px;width: 10px;">email</i>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <nav>
        <div class="nav-wrapper" style="background-color: #024b58;">
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="left hide-on-med-and-down">
                    <li><a href="#">HOME</a></li>
                <li><img src="{{('/images/nav-border.png')}}" style="margin-top: 20px"></li>
                    <li><a href="#">PRIZE BOND</a></li>
                <li><img src="{{('/images/nav-border.png')}}" style="margin-top: 20px"></li>
                <li><a href="#">FORUM</a></li>
                <li><img src="{{('/images/nav-border.png')}}" style="margin-top: 20px"></li>
                <li><a href="#">CHARTS</a></li>
                <li><img src="{{('/images/nav-border.png')}}" style="margin-top: 20px"></li>
                <li><a href="#">COMPANY ANNOUNCEMENT</a></li>
                <li><img src="{{('/images/nav-border.png')}}" style="margin-top: 20px"></li>
                <li><a href="#">ANNUAL REPORTS</a></li>
                <li><img src="{{('/images/nav-border.png')}}" style="margin-top: 20px"></li>
                <li><a href="#">BROKER REPORTS</a></li>
                <li><img src="{{('/images/nav-border.png')}}" style="margin-top: 20px"></li>
                <li><a href="#">DAILY SNAPSHOT</a></li>
                </ul>

                <ul class="side-nav" id="mobile-demo">
                    <li><a href="#">HOME</a></li>
                    <li><a href="#">PRIZE BOND</a></li>
                    <li><a href="#">FORUM</a></li>
                    <li><a href="#">CHARTS</a></li>
                    <li><a href="#">COMPANY ANNOUNCEMENT</a></li>
                    <li><a href="#">ANNUAL REPORTS</a></li>
                    <li><a href="#">BROKER REPORTS</a></li>
                    <li><a href="#">DAILY SNAPSHOT</a></li>
            </ul>
        </div>
    </nav>
</div>

<script>
    $('document').ready(function () {
        $(".button-collapse").sideNav();
    })
</script>