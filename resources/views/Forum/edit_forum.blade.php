@extends('layouts.app')

@section('content')

    {{--{{ Form::open(array('route' => 'user.store')) }}--}}
    {{--{{Form::label('name','Name')}}--}}
    {{--{{Form::text('name')}}--}}
    {{--{{Form::label('email','Email')}}--}}
    {{--{{Form::email('email')}}--}}
    {{--{{Form::label('password','Password')}}--}}
    {{--{{Form::password('password')}}--}}
    {{--{{Form::submit('Add User')}}--}}
    {{--{{ Form::close() }}--}}

    <div class="container-fluid">
        <div class="row text-center">
            <h2 style="color: #024b5a; font-weight: bold;padding: 10px;">Update Forum</h2>
        </div>

        {{ Form::model($forum, ['method' => 'PUT', 'route' => ['forum.update',$forum->id]]) }}

        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12 card" style="background-color: #ffffff;">
                <div class="row" style="padding-left: 15px;padding-right: 15px;margin-bottom: 10px !important;">
                    <div class="input-field col s12" style="margin-top: 30px">
                        <input name="title" style="font-size: 15px;color: #040404 !important;" id="title"
                               value="{{$forum->title}}" type="text"
                               class="validate">
                        <label for="title" style="color: #024b5a !important; font-size: 16px;">Title</label>
                    </div>
                </div>
                <div class="row" style="padding-left: 15px;padding-right: 15px;">
                    <div class="input-field col s12">
                        <textarea style="color: #040404; font-size: 15px;" id="description" name="description">{{$forum->description}}</textarea>

                        <label for="description" style="color: #024b5a;font-size: 16px;top: 0.2rem !important;">Description</label>
                    </div>
                </div>


                <div class="row" style="padding: 15px;">
                    <div class="col s8">
                        <button type="submit" class="btn btn-success"><span class="text-capitalize"
                                                                                                style="font-weight: bold;">Update</span>
                        </button>
                        <a href="/forum">
                            <button type="button" class="btn btn-danger"><span class="text-capitalize"
                                                                               style="font-weight: bold;">Cancel</span>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}


    </div>

    <script src="/vendor/ckeditor/release/ckeditor.js"></script>

    <script>
        CKEDITOR.replace('description');
    </script>
@endsection