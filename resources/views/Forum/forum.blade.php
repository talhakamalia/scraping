@extends('layouts.app')

@section('css')
    <link href="/css/forum.css" rel="stylesheet">
@endsection
@section('content')
    <div class="container-fluid">

        <section>
        <div class="row">
        <h1 style="color: #008f8f" class="text-left">Forum:</h1>
        <a href="{{url('forum/create')}}"><button class="btn btn-link right">Create a new forum</button></a>
        </div>


        <div class="row" style="margin-top: 20px">
        <table class="table table-bordered table-responsive col-md-10 card"
        style="color:#000000;font-weight: bold;">
        <thead>
        <tr style="color: #008f8f;font-weight: bold; font-size: 18px;padding:5px;">
        <th class="text-center">ID</th>
        <th>Title</th>
        <th>Description</th>
        <th>Created at</th>
        <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $forums)
        <tr style="color: #000000; font-weight: bold">
        <td class="text-center">{{$forums -> id}}</td>
        <td><a href="/forum/{{$forums -> id}}">{{$forums -> title}}</a></td>
        <td>{!!$forums -> description!!}</td>
        <td>{{$forums -> created_at}}</td>
        <td class="col s12">
        <span class="col s6" style="margin-top: 15px;"><a style="color: #ffffff; font-weight: bold;" href="/forum/{{$forums->id}}/edit"
        class="btn btn-primary">
        <span class="text-capitalize">Edit</span></a></span>
        <span class="col s6" style="margin-top: 15px;">{{ Form::open(array('url' => 'forum/' . $forums->id, 'id'=>$forums->id,'class' => 'delete')) }}
        {{ Form::hidden('_method', 'DELETE') }}
        <button type="button" class="btn waves-effect waves-light right delete" style="background-color: #c51111 !important;">Delete</button>
        {{ Form::close() }}</span>
        </td>

        </tr>
        @endforeach
        </tbody>
        </table>
        </div>
        </section>


    </div>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".delete").click(function (event) {
                event.preventDefault();
                var id = $(this).attr('id');
//            console.log($(this).attr('id'),'idd')
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function () {
                    $("#" + id).submit();
                }, function (dismiss) {
                    // dismiss can be 'cancel', 'overlay',
                    // 'close', and 'timer'
                    if (dismiss === 'cancel') {
                        swal(
                            'Cancelled',
                            'Your file is safe :)',
                            'error'
                        )
                    }
                })
            });
        })
    </script>
    <script src="/vendor/ckeditor/release/ckeditor.js"></script>

    <script>
        CKEDITOR.replace('description');
    </script>
@endsection

