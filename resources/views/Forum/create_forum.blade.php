@extends('layouts.app')
@section('css')
    <link href="/css/forum.css" rel="stylesheet">
@endsection
@section('content')

    {{--{{ Form::open(array('route' => 'user.store')) }}--}}
    {{--{{Form::label('name','Name')}}--}}
    {{--{{Form::text('name')}}--}}
    {{--{{Form::label('email','Email')}}--}}
    {{--{{Form::email('email')}}--}}
    {{--{{Form::label('password','Password')}}--}}
    {{--{{Form::password('password')}}--}}
    {{--{{Form::submit('Add User')}}--}}
    {{--{{ Form::close() }}--}}

    <div class="container-fluid">
        <section id="forum-section-create">
            <div class="row" style="margin-bottom: 0 !important;">
                <div class="col s12 card" style="background-color: #024b5a;border-bottom: 3px solid #00d3ff">
                    <h4 style="color: white;padding-left: 15px;">Create Thread</h4>
                </div>
            </div>

            {{ Form::open(array('route' => 'forum.store')) }}

            <div class="row">
                <div class="col-md-12 card" style="background-color: #363838;">
                    <div class="row" style="padding-left: 15px;padding-right: 15px;margin-bottom: 10px !important;">
                        <div class="input-field col s12" style="margin-top: 30px">
                            <input name="title" style="font-size: 15px;color: #040404 !important;" id="title"
                                   type="text"
                                   class="validate">
                            <label for="title" style="color: #ffffff !important; font-size: 16px;">Title</label>
                        </div>
                    </div>

                    <div class="row" style="padding-left: 15px;padding-right: 15px;margin-bottom: 10px !important;">
                        <div class="input-field col s12" style="margin-top: 5px">
                            <select id="selector" name="category_id">
                                <option value="" disabled selected>Choose your category</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" style="color:#024b5a;">{{$category->name}}</option>
                                @endforeach
                            </select>
                            <label for="category" style="color: #ffffff !important; font-size: 16px;">Category</label>
                        </div>
                    </div>

                    <div class="row" style="padding-left: 15px;padding-right: 15px;">
                        <div class="input-field col s12">
                        <textarea style="color: #040404; font-size: 15px;" id="description"
                                  name="description"></textarea>
                            <label for="description" style="color: #ffffff;font-size: 16px;top: -1.9rem !important;">Description</label>
                        </div>
                    </div>

                    <div class="row" style="padding: 15px;">
                        <div class="col s12">
                            <button type="submit" href="/forum.store" class="btn btn-success right"><span class="text-capitalize"
                                                                                                    style="font-weight: bold;">Create</span>
                            </button>
                            <a href="/forum">
                                <button type="button" class="btn btn-danger right" style="margin-right: 5px !important;"><span class="text-capitalize"
                                                                                   style="font-weight: bold;">Cancel</span>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}

        </section>



    </div>
    <script src="/vendor/ckeditor/release/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('description');
    </script>
    <script>
        $(document).ready(function() {
            $('select').material_select();
        });
    </script>
@endsection