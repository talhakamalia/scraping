@extends('layouts.app')

@section('css')
    <link href="/css/forum.css" rel="stylesheet">
@endsection
@section('content')
    <div class="container-fluid">

        <section>

            <div id="forum-section" class="row">
                <div class="col s12" style="background-color: #363838;">

                    <div id="forum-section-category" class="row">
                        <div class="col s12" style="border-bottom: 1px solid lightgrey;">
                            <div class="col s12">
                                <h5 style="color: #00deff;">Title: {{$forum->title}}</h5>
                            </div>
                            <div class="col s12">
                                <span style="color: white;">{!! $forum->description !!}</span>
                            </div>
                        </div>

                    </div>
                    <div id="forum-section-category" class="row">
                        <div class="col s12" style="border-bottom: 1px solid #ffffff;">
                            <div class="col s12">
                                <h5 style="color: #00deff">Comments</h5>
                            </div>
                            @foreach($data as $comment)

                                <div class="col s12 row">
                                    <div class="col s12 card valign-wrapper" style="background-color: #363838;">
                                        <div class="col s10">
                                            <h6 style="color: white;padding: 15px">{!! $comment->description !!}</h6>
                                        </div>
                                        <div class="col s10">
                                            <h6 style="color: yellow;padding: 15px">
                                            {!! $comment->reply['description'] !!}
                                            </h6>
                                        </div>
                                       {{--  <div class="col s2 valign">
                                            <button id="reply" type="submit" class="btn btn-link right"
                                                    style="background-color: #00deff">reply
                                            </button>
                                        </div> --}}
                                    </div>

                                    {{ Form::open(array('route' => 'comment.store')) }}

                                        <div class="row">
                                            <input type="text" name="parent_id" class="hide" value="{{$comment->id}}">
                                                <div class="row" style="padding-left: 15px;padding-right: 15px;margin-bottom: 0 !important;">
                                                    <div class="input-field col s12 hidden">
                                                        <input hidden id="forum_id"
                                                               value="{{$forum->id}}" name="forum_id">
                                                    </div>
                                                    <div class="input-field col s12">
                                                    <textarea style="color: #ffffff; font-size: 15px;" id="description"
                                                              name="description" placeholder="Reply here..."></textarea>
                                                        <label for="description"
                                                               style="color: #ffffff;font-size: 16px;top: 1.0rem !important;"></label>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding: 15px;">
                                                    <div class="col s12">
                                                        <button type="submit"class="btn btn-success right"><span
                                                                    class="text-capitalize"
                                                                    style="font-weight: bold;">Reply</span>
                                                        </button>
                                                    </div>
                                                </div>
                                        </div>
                                        {{ Form::close() }}
                                   {{--  <div id="reply-box" class="col s12 card" style="background-color: #363838;">

                                        <div>
                                            <div class="input-field col s12 hidden">
                                                <input hidden id="forum_id"
                                                       value="{{$forum->id}}" name="forum_id">
                                            </div>
                                            <div class="input-field col s12">
                                        <textarea style="color: #ffffff; font-size: 15px;" id="description"
                                        name="description" placeholder="Reply here..."></textarea>
                                                <label for="description"
                                                       style="color: #ffffff;font-size: 16px;top: 1.0rem !important;"></label>
                                            </div>
                                        </div>

                                        <div class="row" style="padding: 15px;">
                                            <div class="col s12">
                                                <button type="submit" class="btn btn-success right"><span
                                                            class="text-capitalize"
                                                            style="font-weight: bold;">Reply</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div> --}}

                                </div>



                                @if ( $comment->replies )
                                    @foreach($comment->replies as $rep1)
                                        {{ $rep1->description }}
                                    @endforeach
                                @endif
                            @endforeach

                        </div>

                    </div>
                </div>

            </div>

        </section>


        <section id="forum-section-create">
            <div class="row" style="margin-bottom: 0 !important;">
                <div class="col s12 card" style="background-color: #024b5a;border-bottom: 3px solid #00d3ff">
                    <h4 style="color: white;padding-left: 15px;">Comment Box</h4>
                </div>
            </div>

            {{ Form::open(array('route' => 'comment.store')) }}

            <div class="row">
                <div class="col-md-12 card" style="background-color: #363838;">

                    <div class="row" style="padding-left: 15px;padding-right: 15px;margin-bottom: 0 !important;">
                        <div class="input-field col s12 hidden">
                            <input hidden id="forum_id"
                                   value="{{$forum->id}}" name="forum_id">
                        </div>
                        <div class="input-field col s12">
                        <textarea style="color: #ffffff; font-size: 15px;" id="description"
                                  name="description" placeholder="Comment here..."></textarea>
                            <label for="description"
                                   style="color: #ffffff;font-size: 16px;top: 1.0rem !important;"></label>
                        </div>
                    </div>

                    <div class="row" style="padding: 15px;">
                        <div class="col s12">
                            <button type="submit"class="btn btn-success right"><span
                                        class="text-capitalize"
                                        style="font-weight: bold;">Comment</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}

        </section>


    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            
            $("#reply").click(function () {
                $("#reply-box").dialog("open");
            });

            $(".delete").click(function (event) {
                event.preventDefault();
                var id = $(this).attr('id');
//            console.log($(this).attr('id'),'idd')
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function () {
                    $("#" + id).submit();
                }, function (dismiss) {
                    // dismiss can be 'cancel', 'overlay',
                    // 'close', and 'timer'
                    if (dismiss === 'cancel') {
                        swal(
                            'Cancelled',
                            'Your file is safe :)',
                            'error'
                        )
                    }
                })
            });
        })
    </script>

@endsection

