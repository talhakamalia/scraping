@extends('layouts.app')
@section('css')
    <link href="/css/forum.css" rel="stylesheet">
@endsection
@section('content')


    <div class="container-fluid">
        <section id="forum-section-create">
            <div class="row" style="margin-bottom: 0 !important;">
                <div class="col s12 card" style="background-color: #024b5a;border-bottom: 3px solid #00d3ff">
                    <h4 style="color: white;padding-left: 15px;">Comment Box</h4>
                </div>
            </div>

            {{ Form::open(array('route' => 'comment.store')) }}

            <div class="row">
                <div class="col-md-12 card" style="background-color: #363838;">

                    <div class="row" style="padding-left: 15px;padding-right: 15px;margin-bottom: 0 !important;">
                        <div class="input-field col s12 hidden">
                        <textarea id="forum_id"
                                  value="{{$forum->id}}" name="forum_id"></textarea>
                        </div>
                        <div class="input-field col s12">
                        <textarea style="color: #ffffff; font-size: 15px;" id="description"
                                  name="description"></textarea>
                            <label for="description" style="color: #ffffff;font-size: 16px;top: 1.0rem !important;">Comment</label>
                        </div>
                    </div>

                    <div class="row" style="padding: 15px;">
                        <div class="col s12">
                            <button type="submit" href="/comment.store" class="btn btn-success right"><span
                                        class="text-capitalize"
                                        style="font-weight: bold;">Create</span>
                            </button>
                            <a href="/forum">
                                <button type="button" class="btn btn-danger right"
                                        style="margin-right: 5px !important;"><span class="text-capitalize"
                                                                                    style="font-weight: bold;">Cancel</span>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}

        </section>


    </div>

@endsection