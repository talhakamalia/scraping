@extends('layouts.app')

@section('css')
    <link href="/css/forum.css" rel="stylesheet">
@endsection
@section('content')
    <div class="container-fluid">

        <section>

            <div class="row" style="margin-bottom: 0 !important;">
                <div class="col s12" style="background-color: #024b5a;border-bottom: 3px solid #00d3ff">
                    <h4 style="color: white;padding-left: 15px;">Main Forums</h4>
                </div>
            </div>
            <div id="forum-section" class="row">
                <div class="col s12" style="background-color: #363838">
                    <div class="col s12">
                        <div class="col s3 right" style="padding: 20px;">
                            <a href="forum/create">
                                <button class="btn waves-effect waves-light"
                                        style="color: #00deff;background-color: #024b5a;">Create Thread
                                </button>
                            </a>
                        </div>
                    </div>

                    <div id="forum-section-category" class="row">
                        @foreach($categories as $category)
                            {{--Member Introduction--}}
                            <div class="col s12" style="border-bottom: 1px solid lightgrey;">
                                <div class="col s2 center" style="margin-top: 2%;">
                                    <i class="material-icons medium" style="color: white">perm_identity</i>
                                </div>
                                <div class="col s10">
                                    <div class="col s12">
                                        <a href="/category/{{$category->id}}"><h5 style="color: #00deff">{{$category->name}}</h5></a>
                                    </div>
                                    <div class="col s12">
                                        <p style="color: white">{{$category->description}}</p>
                                    </div>
                                </div>
                            </div>
                            {{--End--}}

                        @endforeach
                    </div>
                </div>

            </div>

        </section>


        <section>
            <div class="row" style="margin-bottom: 0 !important;">
                <div class="col s12" style="background-color: #024b5a;border-bottom: 3px solid #00d3ff">
                    <h4 style="color: white;padding-left: 15px;">Favorite Topic</h4>
                </div>
            </div>

            <div class="row">

                <div class="col s12" style="background-color: #363838;">

                    <div class="col s12" style="color: white">

                        <div class="col s6 ">
                            <div class="col s2">
                                <i class="material-icons  small" style="margin-top: 10px">grade</i>
                            </div>

                            <div class="col s4">
                                <h5>Imran</h5>
                            </div>
                        </div>

                        <div class="col s6 left">
                            <div class="col s2">
                                <i class="material-icons small" style="margin-top: 10px;">grade</i>
                            </div>

                            <div class="col s4">
                                <h5>Sohaib</h5>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </section>


        <section>
            <div class="row" style="margin-bottom: 0 !important;">
                <div class="col s12" style="background-color: #024b5a;border-bottom: 3px solid #00d3ff">
                    <h4 style="color: white;padding-left: 15px;">Unread Posts</h4>
                </div>
            </div>

            <div class="row">

                <div class="col s12" style="background-color: #363838;">

                    <div class="col s12" style="color: white;border-bottom: 1px solid lightgrey;">

                        <div class="col s12">
                            <div class="col s1">
                                <i class="material-icons small" style="margin-top: 10px">email</i>
                            </div>

                            <div class="col s11">
                                <h5>Imran</h5>
                            </div>
                        </div>

                    </div>

                </div>


            </div>
        </section>


    </div>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".delete").click(function (event) {
                event.preventDefault();
                var id = $(this).attr('id');
//            console.log($(this).attr('id'),'idd')
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function () {
                    $("#" + id).submit();
                }, function (dismiss) {
                    // dismiss can be 'cancel', 'overlay',
                    // 'close', and 'timer'
                    if (dismiss === 'cancel') {
                        swal(
                            'Cancelled',
                            'Your file is safe :)',
                            'error'
                        )
                    }
                })
            });
        })
    </script>
    <script src="/vendor/ckeditor/release/ckeditor.js"></script>

    <script>
        CKEDITOR.replace('description');
    </script>
@endsection

