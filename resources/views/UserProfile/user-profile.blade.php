@extends('layouts.app')
@section('css')
    <link rel="stylesheet" type="text/css" href="/css/userprofile.css">
@endsection
@section('content')
    <div class="row margin-top-20">
        <div class="col s12 pull-right">
            <a class="waves-effect waves-light btn right btn-bg-dark-green">Send Message</a>
        </div>
    </div>

    {{--Main Content--}}

    <div class="row">
        <div class="col s12">
            <div class="col m3 s12 background-color-dark-green">
                <div class="col s12">
                    {{--<div class="col s5 center-block">--}}
                    <img src="{{url('/images/user.png')}}" class="circle responsive-img">
                    {{--</div>--}}
                </div>
            </div>

            <div class="col m9 s12">
                <div class="col s12 background-color-dark-green border-bottom-blue">
                    <h5 class="padding-15px">Latest Posts</h5>
                </div>
                <div class="col s12 background-color-363838 margin-top-minus-10">
                    <div class="padding-15px blue-text font-size-16px">Drupal community involvement</div>
                    <div class="white-text font-size-16px">Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Aspernatur eaque enim error fuga iure laudantium maxime minus nemo nobis omnis
                        perferendis possimus quod quos recusandae repellat repudiandae rerum, tenetur. Similique!
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection