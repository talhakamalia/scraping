@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row text-center" style="margin-top: 20px;">
            <h2 style="color: black;font-weight: bold;">BOND</h2>

            {{--{{Form::open(array('url' => 'create')) }}--}}
            {{--{{Form::submit('Add New Product',array("class"=> "btn-add-user"))}}--}}
            {{--{{Form::close() }}--}}
            <a href="/bond/create"><button type="button">Add New Bond</button> </a>

        </div>

        <div class="row text-left">
            <h3 style="color: #ffffff">Bond :</h3>
        </div>


        <div class="row" style="margin-top: 20px">
            <table class="table table-bordered table-responsive col-md-10 card"
                   style="color:#000000;font-weight: bold;">
                <thead>
                <tr style="color: #238c59;font-weight: bold; font-size: 18px;">
                    <th class="text-center">ID</th>
                    <th>To</th>
                    <th>From</th>
                    <th>Category</th>
                    <th>user_id</th>
                </tr>
                </thead>
                <tbody>
                @foreach($bond as $bond)
                    <tr style="color: #000000; font-weight: bold">
                        <td class="text-center">{{$bond -> id}}</td>
                        <td>{{$bond -> to}}</td>
                        <td>{{$bond -> from}}</td>
                        <td>{{$bond -> category}}</td>
                        <td>{{$bond -> created_at}}</td>
                        <td>
                            <div class="col s12">
                                <div class="col s6">
                            <a style="color: #ffffff; font-weight: bold;" href="/bond/{{$bond->id}}/edit"
                               class="btn btn-primary"><span style="font-size: 14px;"
                                                             class="text-capitalize">Edit</span></a>
                                </div>
                                <div class="col s6">
                            {{ Form::open(array('url' => 'bond/' . $bond->id)) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                            {{ Form::close() }}
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

@endsection
