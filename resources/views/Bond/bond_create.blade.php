@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row text-center">
            <h2 style="color: #238c59; font-weight: bold;padding: 10px;">Create Bond</h2>
        </div>

        {{ Form::open(array('route' => 'bond.store')) }}

        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12 card" style="background-color: #ffffff;">
                <div class="row" style="padding-left: 15px;">
                    <div class="input-field col s12" style="margin-top: 30px;">
                        <input name="to" style="font-size: 14px;color: #040404 !important;" id="to" type="text"
                               class="validate">
                        <label for="to" style="color: #238c59 !important; font-size: 14px;">To</label>
                    </div>
                </div>
                <div class="row" style="padding-left: 15px;">
                    <div class="input-field col s12">
                        <input name="from" style="color: #040404; font-size: 14px;" id="from" type="text"
                               class="validate">
                        <label for="from" style="color: #238c59;font-size: 14px;">From</label>
                    </div>
                </div>
                <div class="row" style="padding-left: 15px;">
                    <div class="input-field col s12">
                        {{--<input name="category" style="color: #040404; font-size: 14px;" id="category" type="text"--}}
                        {{--class="validate">--}}
                        <select name="category" style="color: #040404; font-size: 14px;" id="category" type="text"
                                class="validate">
                            <option value="" disabled selected>Choose your bond</option>
                            <option value="100">100</option>
                            <option value="200">200</option>
                            <option value="750">750</option>
                            <option value="1,500">1,500</option>
                            <option value="7,500">7,500</option>
                            <option value="15,000">15,000</option>
                            <option value="25,000">25,000</option>
                            <option value="40,000">40,000</option>
                        </select>
                        <label for="category" style="color: #238c59;font-size: 14px;">Category</label>
                    </div>
                </div>


                <div class="row" style="padding: 15px;">
                    <div class="col s4">
                        <button type="submit" href="/bond.store" class="btn btn-success"><span class="text-capitalize"
                                                                                               style="font-weight: bold;">Create</span>
                        </button>
                        <a href="/bond">
                            <button type="button" class="btn btn-danger"><span class="text-capitalize"
                                                                               style="font-weight: bold;">Cancel</span>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}

    </div>
    <script>
        $(document).ready(function () {
            $('select').material_select();
        });
    </script>
@endsection

