<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//FORUM ROUTE

Route::resource('/forum','ForumController');
Route::get('/category','ForumController@category');
Route::get('/category/{id}','ForumController@view');



//Route::get('/forum/destroy/{{id}}','ForumController@destroy');

//Comment Route

Route::post('/comment','CommentController@store')->name('comment.store');
Route::post('/comment/reply','CommentController@reply')->name('comment.reply');

//BOND ROUTE

Route::resource('/bond','BondController');
//Route::get('/bond','BondController@index');
//Route::resource('/create','BondController@create');
//Route::get('/bonds/{id}/edit','BondController@edit');
//Route::get('/bond/destroy/{{id}}','BondController@destroy');


Route::get('/home','HomeController@index');
Route::resource('/scrap','ScrapingController');
Auth::routes();

//USER PROFILE

Route::resource('/user-profile','UserProfileController');