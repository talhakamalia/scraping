<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('categories')->insert([

            [
                'name' => 'Member Introduction',
                'description' => 'ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur eligendi itaque neque recusandae! Asperiores assumenda cupiditate eligendi ex ipsam maxime, minus qui quo reiciendis repellat! Labore officia qui temporibus!'
            ],
            [
                'name' => 'General Market Discussion',
                'description' => 'ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur eligendi itaque neque recusandae! Asperiores assumenda cupiditate eligendi ex ipsam maxime, minus qui quo reiciendis repellat! Labore officia qui temporibus!'

            ],
            [
                'name' => 'Share Specific Discussion',
                'description' => 'ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur eligendi itaque neque recusandae! Asperiores assumenda cupiditate eligendi ex ipsam maxime, minus qui quo reiciendis repellat! Labore officia qui temporibus!'

            ],
            [
                'name' => 'Brokers Discussion',
                'description' => 'ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur eligendi itaque neque recusandae! Asperiores assumenda cupiditate eligendi ex ipsam maxime, minus qui quo reiciendis repellat! Labore officia qui temporibus!'

            ],
            [
                'name' => 'Prize Bond Discussion',
                'description' => 'ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur eligendi itaque neque recusandae! Asperiores assumenda cupiditate eligendi ex ipsam maxime, minus qui quo reiciendis repellat! Labore officia qui temporibus!'

            ],
            [
                'name' => 'Commodity Discussion',
                'description' => 'ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur eligendi itaque neque recusandae! Asperiores assumenda cupiditate eligendi ex ipsam maxime, minus qui quo reiciendis repellat! Labore officia qui temporibus!'

            ],
            [
                'name' => 'Forex Discussion',
                'description' => 'ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur eligendi itaque neque recusandae! Asperiores assumenda cupiditate eligendi ex ipsam maxime, minus qui quo reiciendis repellat! Labore officia qui temporibus!'

            ],
            [
                'name' => 'Property Discussion',
                'description' => 'ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur eligendi itaque neque recusandae! Asperiores assumenda cupiditate eligendi ex ipsam maxime, minus qui quo reiciendis repellat! Labore officia qui temporibus!'

            ],
            [
                'name' => 'Miscellaneous Discussion',
                'description' => 'ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur eligendi itaque neque recusandae! Asperiores assumenda cupiditate eligendi ex ipsam maxime, minus qui quo reiciendis repellat! Labore officia qui temporibus!'

            ],
        ]);
    }
}
