<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Reply;
use Illuminate\Http\Request;
use App\Forum;
use Auth;
use Riazxrazor\LaravelSweetAlert\LaravelSweetAlert;


class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Forum::where('active','1')->get();
        //ye phr chuss mari hui? ye all() dobara se q kiya hua ? uper where()->get() lagany se data ajata.. all lagany se sara data ajaata jo active nhi b wo b ajai ga 
        // $data = $forum->all();

        return view ('/Forum/forum' , compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        // yaha pe tuny data k variable avin pass kiya hua ta uski zaroret nhi ti
        return view ('/Forum/create_forum',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add(['user_id'=>Auth::id()]);
//        dd($request->all());
        Forum::create($request->all());
        LaravelSweetAlert::setMessageSuccessConfirm("Forum Created Successfully!");

        return redirect('/forum');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $forum = Forum::findorfail($id);
        //yaha b tuny comments->all() kiya hua ta... is tara sary comments show hujaingy but hamny sirf wohi comments show karany jo is forum k uper hui hui.
        $data = Comment::where('forum_id',$id)->get();
        return view('/Forum/story',compact('forum','data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $forum = Forum::findorfail($id);
        return view('/Forum/edit_forum', compact('forum'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $forum = Forum::findorfail($id);
        $this->validate($request , ['title'=>'required','description'=>'required']);
        $input = $request->all();
        $forum->fill($input)->save();
        LaravelSweetAlert::setMessageSuccess("Forum Updated Successfully");

        return redirect('/forum');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $forum = Forum::findorfail($id);
        $forum->active=0;
        $forum->save();
        LaravelSweetAlert::setMessageSuccess("Forum Deleted Successfully");


        return redirect('/forum');
    }

    public function category()
    {
        $categories = Category::all();
        return view('/Forum/main',compact('categories'));
    }

    public function view($id)
    {

        $data=Forum::where('category_id',$id)->get();

        //Ye forum all q kiya hua ?? uper wali logic se ham sirf wohi forum(thread) ly k arahy 
        //jo us category se belongs krty... forum all se sary threadss ajai gy jo is category k nhi b 

        // $data=$forum->all();

        return view('/Forum/forum',compact('data'));
    }

}
