<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Reply;
use Auth;
use Riazxrazor\LaravelSweetAlert\LaravelSweetAlert;



class CommentController extends Controller
{
    public function index($id)
    {
        $data = Comment::all();
        $forum = Forum::findorfail($id);

        return view ('/Forum/story' , compact('data','forum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add(['user_id'=>Auth::id()]);
//        dd($request->all());
        Comment::create($request->all());
        LaravelSweetAlert::setMessageSuccessConfirm("Commented Successfully!");

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = Comment::findorfail($id);
        return view('/Forum/story', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::findorfail($id);
        $this->validate($request , ['description'=>'required']);
        $input = $request->all();
        $comment->fill($input)->save();
        LaravelSweetAlert::setMessageSuccess("Forum Updated Successfully");

        return redirect('/forum/story');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $forum = Comment::findorfail($id);
        $forum->active=0;
        $forum->save();
        LaravelSweetAlert::setMessageSuccess("Forum Deleted Successfully");


        return redirect('/forum/story');
    }

}
