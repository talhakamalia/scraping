<?php

namespace App\Http\Controllers;

use App\Bond;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BondController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $bond=Bond::where('active','1')->get();
        return view('Bond.bond',compact('bond'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Bond.bond_create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->request->add(['user_id'=>Auth::id()]);
        Bond::create ($request->all());
            return redirect ('/bond');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $bond = Bond::findorfail($id);
        return view('Bond.edit_bond', compact('bond'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $bond = Bond::findorfail($id);
//        dd('$bond');
        $this->validate($request, ['to'=>'required','from'=>'required','category'=>'required']);
        $input = $request->all();
        $bond->fill($input)->save();
        return redirect('/bond');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $bond=Bond::findorfail($id);
        $bond->active=0;
        $bond->save();
        return redirect('/bond');
    }
}
