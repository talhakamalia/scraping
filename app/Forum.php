<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    //

    protected $fillable = [
        'title', 'description', 'user_id','active','category_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    public function comments() {
        return $this->hasMany('App\Comment','comment_id');
    }

    public function category() {
        return $this->belongsTo('App\Category','category_id');
    }
}
