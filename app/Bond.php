<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bond extends Model
{
    //

    protected $fillable = [
        'to', 'from','category', 'user_id','active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
