<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $fillable = [
        'description','active','user_id','forum_id'
    ];

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function forum() {
        return $this->belongsTo('App\Forum');
    }

    public function replies() {
        return $this->hasMany('App\Comment', 'parent_id');
    }
}
