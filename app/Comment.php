<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = [
        'description','active','user_id','forum_id','parent_id'
    ];

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function forum() {
        return $this->belongsTo('App\Forum');
    }
    public function reply(){
    	// $data=Comment::where('parent_id',$id)->first();
    	return $this->belongsTo(Comment::class, 'parent_id') ;
    }
}
